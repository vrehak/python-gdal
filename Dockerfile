FROM python:3.5.1

RUN apt-get update && \
    apt-get install -qy --no-install-recommends python gdal-bin python-gdal && \
    rm -rf /var/lib/apt/lists/*
